package com.example.demo_mybatis.controller;

import com.example.demo_mybatis.model.entity.Product;
import com.example.demo_mybatis.model.request.ProductRequest;
import com.example.demo_mybatis.model.response.ProductResponse;
import com.example.demo_mybatis.repostory.ProductRepository;
import com.example.demo_mybatis.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v2/products")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {

        this.productService = productService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all products")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct() {
        ProductResponse<List<Product>> response= ProductResponse.<List<Product>>builder()
                .message("Fetch Successfully")
                .payload(productService.getAllProducts())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Products By Id")

    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer proId) {

        if (productService.getProductById(proId)!=null) {
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Checking Successfully")
                    .payload(productService.getProductById(proId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);

        }
        else {
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Checking not found")
                    .payload(productService.getProductById(proId))
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);

        }
    }


    @DeleteMapping("/{id}")
    @Operation(summary = "Deleted product through Id")
    public  ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("id") Integer proId){
    ProductResponse<String> response= null;
        if (productService.deleteProductById(proId)==true) {
            response= ProductResponse.<String>builder()
                    .message("Deleted Successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response= ProductResponse.<String>builder()
                    .message("ID Not Found")
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }

    }

@PostMapping
    @Operation(summary = "Insert New Products")
    public  ResponseEntity<ProductResponse<Product>> addProduct(@RequestBody ProductRequest productRequest){
        Integer addId= productService.addProduct(productRequest);
        if (addId!=null){
            ProductResponse<Product> response= ProductResponse.<Product>builder()
                    .message("Insert Successfully")
                    .payload(productService.getProductById(addId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);

        }
        return  null;
}

@PutMapping("/{id}")
    @Operation(summary = "Update Products")
    public  ResponseEntity<ProductResponse<Product>> updateProduct(@RequestBody ProductRequest productRequest,@PathVariable("id") Integer proId){

        ProductResponse<Product> response= null;
        Integer updateProducts= productService.updateProduct(productRequest, proId);

        if(updateProducts!=null){
            response = ProductResponse.<Product>builder()
                    .message("Update Successfully")
                    .payload(productService.getProductById(updateProducts))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return  ResponseEntity.ok(response);

        }

        else {
            response = ProductResponse.<Product>builder()
                    .message("Update UnSuccessfully")
                    .payload(productService.getProductById(updateProducts))
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return  ResponseEntity.badRequest().body(response);

        }

}


}
