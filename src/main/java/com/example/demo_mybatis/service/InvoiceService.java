package com.example.demo_mybatis.service;

import com.example.demo_mybatis.model.entity.Invoice;
import com.example.demo_mybatis.model.request.InvoiceRequest;
import com.example.demo_mybatis.model.request.ProductRequest;

import java.util.List;

public interface InvoiceService {

    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer voiceId);

    boolean deleteInvoiceById(Integer voiceId);

    Integer addInvoice(InvoiceRequest invoiceRequest);

    Integer updateInvoice(InvoiceRequest invoiceRequest, Integer voiceId);
}
