package com.example.demo_mybatis.service.InvoiceServiceImplement;

import com.example.demo_mybatis.model.entity.Invoice;
import com.example.demo_mybatis.model.request.InvoiceRequest;
import com.example.demo_mybatis.repostory.InvoiceRepository;
import com.example.demo_mybatis.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {

        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer voiceId) {
        return invoiceRepository.getInvoiceById(voiceId);
    }

    @Override
    public boolean deleteInvoiceById(Integer voiceId) {
        return invoiceRepository.deleteInvoiceById(voiceId);
    }

    @Override
    public Integer addInvoice(InvoiceRequest invoiceRequest) {
        Integer voiceId= invoiceRepository.addInvoice(invoiceRequest);
        return voiceId;
    }

    @Override
    public Integer updateInvoice(InvoiceRequest invoiceRequest, Integer voiceId) {
        Integer invoiceUpdate= invoiceRepository.updateInvoice(invoiceRequest, voiceId);
        return invoiceUpdate;
    }


}
