package com.example.demo_mybatis.model.request;

import com.example.demo_mybatis.model.entity.Customer;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerRequest {
        private  String name;
        private  String address;
        private Integer phone;

    }



