package com.example.demo_mybatis.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Customer {
    private  Integer id;
    private  String name;
    private  String address;
    private Integer phone;
}
