package com.example.demo_mybatis.repostory;

import com.example.demo_mybatis.model.entity.Product;
import com.example.demo_mybatis.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("SELECT * FROM products")
    List<Product> findAllProduct();

    @Select("SELECT * FROM products WHERE id=#{proId}")
    Product getProductById(Integer proId);


    @Delete("DELETE  FROM products WHERE id=#{proId}")
    boolean deleteProductById(Integer proId);

    @Select("INSERT INTO products (name, price) VALUES(#{request.name},#{request.price})" +
            "RETURNING id")
    Integer addProduct(@Param("request") ProductRequest productRequest);

    @Select("UPDATE  products " +
            "SET name= #{request.name}, " +
            "price= #{request.price} " +
            "WHERE id= #{proId} " +
            "RETURNING id")
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer proId);
}