package com.example.demo_mybatis.controller;

import com.example.demo_mybatis.model.entity.Customer;
import com.example.demo_mybatis.model.entity.Product;
import com.example.demo_mybatis.model.request.CustomerRequest;
import com.example.demo_mybatis.model.request.ProductRequest;
import com.example.demo_mybatis.model.response.CustomerResponse;
import com.example.demo_mybatis.model.response.ProductResponse;
import com.example.demo_mybatis.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")

public class CustomerController {

    private  final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/allCustomer")
    @Operation(summary = "Get all Customers")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer() {

        CustomerResponse<List<Customer>> response= CustomerResponse.<List<Customer>>builder()
                .message("Fetch Successfully")
                .payload(customerService.getAllCustomer())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);

    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable ("id") Integer cusId){
        if (customerService.getCustomerById(cusId)!=null) {
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Get By ID Succesfully")
                    .payload(customerService.getCustomerById(cusId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{

            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Could not find this ID !")
                    .payload(customerService.getCustomerById(cusId))
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }

    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Deleted Data through ID")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("id") Integer cusId){
        CustomerResponse<String> response=null;
        if(customerService.deleteCustomerById(cusId)==true){
            response= CustomerResponse.<String>builder()
                    .message("Deleted Successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);

        }else {
            response= CustomerResponse.<String>builder()
                    .message("It has no this ID")
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);

        }


    }

    @PostMapping
    @Operation(summary = "Insert New Customers")
    public  ResponseEntity<CustomerResponse<Customer>> insertNewProduct(@RequestBody CustomerRequest customerRequest){
        Integer addId= customerService.insertNewCustomer(customerRequest);
        if (addId !=null){
            CustomerResponse<Customer> response= CustomerResponse.<Customer>builder()
                    .message("Insert Successfully")
                    .payload(customerService.getCustomerById(addId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);

        }
        return  null;
    }
    @PutMapping("/{id}")
    @Operation(summary = "Update Customer")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomer(@RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer cusId){

        CustomerResponse<Customer>response=null;
        Integer updateCus= customerService.updateCustomer(customerRequest, cusId);
        if (updateCus!=null){
            response= CustomerResponse.<Customer>builder()
                    .message("Update Successfully")
                    .payload(customerService.getCustomerById(updateCus))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response= CustomerResponse.<Customer>builder()
                    .message("Incorrect Update !")
                    .payload(customerService.getCustomerById(updateCus))
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }

    }
}








