package com.example.demo_mybatis.repostory;

import com.example.demo_mybatis.model.entity.InvoiceDetail;
import com.example.demo_mybatis.model.entity.Product;
import com.example.demo_mybatis.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceDetailRepository {
    @Select("SELECT * FROM invoices_detail WHERE id=#{voiceId}")
    @Result(property = "customer", column = "customer_id",
    one = @One(select = "com.example.demo_mybatis.repostory.CustomerRepository.getCustomerById")
    )
    InvoiceDetail getInvoiceDetailById(Integer voiceId);

    Integer addInvoice(@Param("request") InvoiceRequest invoiceRequest);
}
