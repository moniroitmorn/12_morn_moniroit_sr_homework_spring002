package com.example.demo_mybatis.service;

import com.example.demo_mybatis.model.entity.Customer;
import com.example.demo_mybatis.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {


    List<Customer> getAllCustomer();


    Customer getCustomerById(Integer cusId);

  boolean deleteCustomerById(Integer cusId);

  Integer insertNewCustomer(CustomerRequest customerRequest);

  Integer updateCustomer(CustomerRequest customerRequest, Integer cusId);

}
