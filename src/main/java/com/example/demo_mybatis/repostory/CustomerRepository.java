package com.example.demo_mybatis.repostory;

import com.example.demo_mybatis.model.entity.Customer;
import com.example.demo_mybatis.model.request.CustomerRequest;
import com.example.demo_mybatis.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT*FROM customers")
    List<Customer> findAllCustomer();

    @Select("SELECT * FROM customers WHERE id=#{id}")
    Customer getCustomerById(Integer cusId);

    @Delete("DELETE FROM customers WHERE id=#{cusId}")
    boolean deleteCustomerById(Integer cusId);

    @Select("INSERT INTO customers (name, address, phone) VALUES(#{request.name},#{request.address},#{request.phone}) " +
            "RETURNING id")
    Integer insertNewCustomer(@Param("request") CustomerRequest customerRequest);

    @Select("UPDATE  customers " +
            "SET name= #{request.name}, " +
            "address= #{request.address}, " +
            "phone= #{request.phone} " +
            "WHERE id= #{cusId} " +
            "RETURNING id")
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer cusId);
}
