package com.example.demo_mybatis.controller;

import com.example.demo_mybatis.model.entity.Invoice;
import com.example.demo_mybatis.model.entity.Product;
import com.example.demo_mybatis.model.request.InvoiceRequest;
import com.example.demo_mybatis.model.request.ProductRequest;
import com.example.demo_mybatis.model.response.InvoiceResponse;
import com.example.demo_mybatis.model.response.ProductResponse;
import com.example.demo_mybatis.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v2/invoices")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    @GetMapping("/allInvoice")
    @Operation(summary = "Get All Invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(){
        InvoiceResponse<List<Invoice>> response= InvoiceResponse.<List<Invoice>>builder()
                .message("Checking Successfully")
                .payload(invoiceService.getAllInvoice())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return  ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    @Operation(summary = "Get  Invoice ById")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer voiceId){
        if (invoiceService.getInvoiceById(voiceId)!=null) {
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("Checking Successfully")
                    .payload(invoiceService.getInvoiceById(voiceId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("It has Data for this ID")
                    .payload(invoiceService.getInvoiceById(voiceId))
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
    @DeleteMapping("/{id}")
    @Operation(summary = "Deleted Invoice through Id")
    public  ResponseEntity<InvoiceResponse<String>> deleteInvoiceById(@PathVariable("id") Integer voiceId) {
        InvoiceResponse<String> response = null;
        if (invoiceService.deleteInvoiceById(voiceId) == true) {
            response = InvoiceResponse.<String>builder()
                    .message("Deleted Successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        } else {
            response = InvoiceResponse.<String>builder()
                    .message("ID Not Found")
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
    @PostMapping
    @Operation(summary = "Insert New Invoice")
    public  ResponseEntity<InvoiceResponse<Invoice>> addInvoice(@RequestBody InvoiceRequest invoiceRequest){
        Integer addInvoice= invoiceService.addInvoice(invoiceRequest);
        if (addInvoice!=null){
            InvoiceResponse<Invoice> response= InvoiceResponse.<Invoice>builder()
                    .message("Insert Successfully")
                    .payload(invoiceService.getInvoiceById(addInvoice))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);

        }
        return  null;
    }
    @PutMapping("/{id}")
    @Operation(summary = "Update Invoice")
    public  ResponseEntity<InvoiceResponse<Invoice>> updateInvoice(@RequestBody InvoiceRequest invoiceRequest,@PathVariable("id") Integer voiceId) {

        InvoiceResponse<Invoice> response = null;
        Integer updateInvoice = invoiceService.updateInvoice(invoiceRequest, voiceId);

        if (updateInvoice != null) {
            response = InvoiceResponse.<Invoice>builder()
                    .message("Update Successfully")
                    .payload(invoiceService.getInvoiceById(updateInvoice))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return ResponseEntity.ok(response);

        } else {
            response = InvoiceResponse.<Invoice>builder()
                    .message("Update UnSuccessfully")
                    .payload(invoiceService.getInvoiceById(updateInvoice))
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return ResponseEntity.badRequest().body(response);

        }
    }


}
