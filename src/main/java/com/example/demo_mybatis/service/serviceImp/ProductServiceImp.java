package com.example.demo_mybatis.service.serviceImp;

import com.example.demo_mybatis.model.entity.Product;
import com.example.demo_mybatis.model.request.ProductRequest;
import com.example.demo_mybatis.repostory.ProductRepository;
import com.example.demo_mybatis.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImp implements ProductService {

    private  final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {

        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer proId) {
        return productRepository.getProductById(proId);
    }

    @Override
    public boolean deleteProductById(Integer proId) {
        return productRepository.deleteProductById(proId);
    }

    @Override
    public Integer addProduct(ProductRequest productRequest) {
        Integer proId= productRepository.addProduct(productRequest);
        return proId;
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer proId) {
        Integer proUpdate= productRepository.updateProduct(productRequest, proId);
        return proUpdate;
    }


}
