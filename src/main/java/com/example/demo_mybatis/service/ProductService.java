package com.example.demo_mybatis.service;

import com.example.demo_mybatis.model.entity.Product;
import com.example.demo_mybatis.model.request.ProductRequest;

import java.util.List;

public interface ProductService {

//    Get All products
    List<Product> getAllProducts();

//    Get product By ID
    Product getProductById (Integer proId);

//    Deleted Product By ID
    boolean deleteProductById(Integer proId);

//    Insert Product By ID
    Integer addProduct(ProductRequest productRequest);

//    Update Product By ID

    Integer updateProduct(ProductRequest productRequest, Integer proId);





}
