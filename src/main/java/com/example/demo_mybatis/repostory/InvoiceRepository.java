package com.example.demo_mybatis.repostory;

import com.example.demo_mybatis.model.entity.Invoice;
import com.example.demo_mybatis.model.request.InvoiceRequest;
import com.example.demo_mybatis.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Properties;

@Mapper
public interface InvoiceRepository {

    @Select("SELECT * FROM invoices")
    @Results(
            id = "invoiceMapper",
            value  ={
                    @Result(property = "id", column = "id"),
                    @Result(property = "product", column = "product_id",
                            one = @One(select = "com.example.demo_mybatis.repostory.ProductRepository.getProductById")
                    ),
                    @Result(property = "invoiceDetail", column = "invoice_id",
                    one = @One(select = "com.example.demo_mybatis.repostory.InvoiceDetailRepository.getInvoiceDetailById")

            )
    }
    )

    List<Invoice> findAllInvoice();

    @Select("SELECT * FROM invoices WHERE id=#{voiceId}")
    @ResultMap("invoiceMapper")
    Invoice getInvoiceById( Integer voiceId);


    @Delete("DELETE  FROM invoices WHERE id=#{voiceId}")
    boolean deleteInvoiceById(Integer voiceId);

    @Select("INSERT INTO invoices (product_id, invoice_id) VALUES(#{request.product_id},#{request.customer_id})" +
            "RETURNING id")
    Integer addInvoice(@Param("request") InvoiceRequest invoiceRequest);

    @Select("UPDATE  invoices " +
            "SET product_id= #{request.product_id}, " +
            "invoice_id= #{request.customer_id} " +
            "WHERE id= #{voiceId} " +
            "RETURNING id")
    Integer updateInvoice(@Param("request") InvoiceRequest invoiceRequest, Integer voiceId);
}
