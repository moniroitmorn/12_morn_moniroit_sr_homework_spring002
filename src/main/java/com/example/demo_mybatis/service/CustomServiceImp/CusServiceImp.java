package com.example.demo_mybatis.service.CustomServiceImp;

import com.example.demo_mybatis.model.entity.Customer;
import com.example.demo_mybatis.model.request.CustomerRequest;
import com.example.demo_mybatis.model.request.ProductRequest;
import com.example.demo_mybatis.repostory.CustomerRepository;
import com.example.demo_mybatis.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CusServiceImp implements CustomerService {

       private final CustomerRepository customerRepository;

    public CusServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer cusId) {

        return customerRepository.getCustomerById(cusId);
    }

    @Override
    public boolean deleteCustomerById(Integer cusId) {
        return customerRepository.deleteCustomerById(cusId);
    }

    @Override
    public Integer insertNewCustomer(CustomerRequest customerRequest) {
        Integer cusId= customerRepository.insertNewCustomer(customerRequest);
        return cusId;

    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer cusId) {

        Integer updateCus = customerRepository.updateCustomer(customerRequest, cusId);
        return updateCus ;
    }


}
